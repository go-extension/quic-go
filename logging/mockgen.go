package logging

//go:generate sh -c "go run go.uber.org/mock/mockgen -package logging -self_package gitlab.com/go-extension/quic-go/logging -destination mock_connection_tracer_test.go gitlab.com/go-extension/quic-go/logging ConnectionTracer"
//go:generate sh -c "go run go.uber.org/mock/mockgen -package logging -self_package gitlab.com/go-extension/quic-go/logging -destination mock_tracer_test.go gitlab.com/go-extension/quic-go/logging Tracer"
