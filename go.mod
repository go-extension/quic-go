module gitlab.com/go-extension/quic-go

go 1.20

require (
	github.com/daeuniverse/softwind v0.0.0-20230903121035-afc8c5d27a4c
	github.com/francoispqt/gojay v1.2.13
	github.com/golang/mock v1.6.0
	github.com/onsi/ginkgo/v2 v2.12.0
	github.com/onsi/gomega v1.27.10
	github.com/quic-go/qpack v0.4.0
	github.com/quic-go/quic-go v0.38.1
	gitlab.com/go-extension/http v0.0.0-20230815073544-790fdd63e91f
	gitlab.com/go-extension/tls v0.0.0-20230910071540-0707051306cc
	go.uber.org/mock v0.2.0
	golang.org/x/crypto v0.13.0
	golang.org/x/exp v0.0.0-20230905200255-921286631fa9
	golang.org/x/net v0.15.0
	golang.org/x/sync v0.3.0
	golang.org/x/sys v0.12.0
)

require (
	github.com/RyuaNerin/go-krypto v1.2.0 // indirect
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/blang/semver/v4 v4.0.0 // indirect
	github.com/go-logr/logr v1.2.4 // indirect
	github.com/go-task/slim-sprig v0.0.0-20230315185526-52ccab3ef572 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/google/pprof v0.0.0-20230907193218-d3ddc7976beb // indirect
	github.com/klauspost/compress v1.16.7 // indirect
	github.com/pedroalbanese/camellia v0.0.0-20220911183557-30cc05c20118 // indirect
	github.com/quic-go/qtls-go1-20 v0.3.3 // indirect
	gitlab.com/go-extension/aes-ccm v0.0.0-20230221065045-e58665ef23c7 // indirect
	golang.org/x/mod v0.12.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	golang.org/x/tools v0.13.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
