# A QUIC extension implementation in pure Go

This repository is an extension of the quic-go library [github.com/quic-go/quic-go](https://pkg.go.dev/github.com/quic-go/quic-go)
<img src="docs/quic.png" width=303 height=124>

# Features

- [Extension TLS](gitlab.com/go-extension/tls)


# Special thanks

- [QUIC-Go](https://github.com/quic-go/quic-go)
- [softwind](https://github.com/daeuniverse/softwind)